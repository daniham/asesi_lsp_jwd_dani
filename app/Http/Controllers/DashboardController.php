<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Beasiswa;

class DashboardController extends Controller
{

    public function index()
    {
        $beasiswa = Beasiswa::all();
 
        return view('welcome',compact('beasiswa'));
    }
}