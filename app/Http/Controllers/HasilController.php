<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RegisterBeasiswa;
use App\Models\Beasiswa;

use Yajra\DataTables\Datatables;
use PDF;

class HasilController extends Controller
{
    public function index()
    {
        $data = RegisterBeasiswa::latest()->paginate(5);

        return view('register-beasiswa.hasil', compact('data'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

}
