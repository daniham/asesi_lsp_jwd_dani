<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Beasiswa;
use Yajra\DataTables\Datatables;
use PDF;

class BeasiswaController extends Controller
{
    public function json(){
        return Datatables::of(Beasiswa::all())->make(true);
    }

    public function index()
    {
        $data = Beasiswa::latest()->paginate(5);

        return view('beasiswa.index', compact('data'))->with('i', (request()->input('page', 1) - 1) * 5);
    }
    public function create()
    {
        return view('beasiswa.create');
    }
    public function store(Request $request)
    {
        $request->validate([
        'beasiswa_name' => 'required|unique:beasiswas',
        'deskripsi' => 'required',
        'jenis' => 'required',
        'donatur' => 'required'
        ]);
        $beasiswa = new Beasiswa;
        $beasiswa->beasiswa_name = $request->beasiswa_name;
        $beasiswa->deskripsi = $request->deskripsi;
        $beasiswa->jenis = $request->jenis;
        $beasiswa->donatur = $request->donatur;
        $beasiswa->save();
        return redirect()->route('beasiswa.index')
        ->with('sukses','Data Master Beasiswa telah Ditambahkan.');
    }

    public function show(Beasiswa $beasiswa)
    {
        return view('beasiswa.show',compact('beasiswa'));
    }
    
    public function edit(Beasiswa $beasiswa)
    {
        return view('beasiswa.edit',compact('beasiswa'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'beasiswa_name' => 'required|unique:beasiswas',
            'deskripsi' => 'required',
            'jenis' => 'required',
            'donatur' => 'required'
            ]);
        $beasiswa = Beasiswa::find($id);
        $beasiswa->beasiswa_name = $request->beasiswa_name;
        $beasiswa->deskripsi = $request->deskripsi;
        $beasiswa->jenis = $request->jenis;
        $beasiswa->donatur = $request->donatur;
        $beasiswa->save();
        return redirect()->route('beasiswa.index')
        ->with('sukses','Data Beasiswa Telah Berhasil Di Update');
    }

    public function destroy(Beasiswa $beasiswa)
    {
        $beasiswa->delete();
            return redirect()->route('beasiswa.index')
        ->with('sukses','Data Beasiswa Telah berhasil Di delete');
    }

    public function cetak_pdf()
    {
    	$beasiswa = Beasiswa::all();
 
    	$pdf = PDF::loadview('beasiswa.report-beasiswa',['beasiswa'=>$beasiswa]);
    	return $pdf->download('laporan-beasiswa-pdf');
    }

    public function search(Request $request)
    {
        $keyword = $request->search;
        $data = Beasiswa::where('beasiswa_name', 'like', "%" . $keyword . "%")
        ->orWhere('jenis', 'like', "%" . $keyword . "%")
        ->orWhere('donatur', 'like', "%" . $keyword . "%")->paginate(5);
        return view('beasiswa.index', compact('data'))->with('i', (request()->input('page', 1) - 1) * 5);
    }
}
