<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RegisterBeasiswa;
use App\Models\Beasiswa;

use Yajra\DataTables\Datatables;
use PDF;
class RegisterBeasiswaController extends Controller
{
    public function index()
    {
        $data = RegisterBeasiswa::latest()->paginate(5);

        return view('register-beasiswa.index', compact('data'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        $beasiswa = Beasiswa::all();
        
        return view('register-beasiswa.create', compact('beasiswa'));
    }

    public function store(Request $request)
    {
        $request->validate([
        'name' => 'required|unique:register_beasiswas',
        'email' => 'required|regex:/(.+)@(.+)\.(.+)/i',
        'nomorHp' => 'required|numeric|digits:12',
        'semester' => 'required',
        'beasiswa_id' => 'required',
        'file' => 'required|mimes:csv,txt,xlx,xls,pdf,jpg,jpeg,zip|max:2048'
        ]);
        if($request->file()) {
            $fileName = time().'_'.$request->file->getClientOriginalName();
            $filePath = $request->file('file')->storeAs('uploads', $fileName, 'public');
            $register = new RegisterBeasiswa;
            $register->name = $request->name;
            $register->email = $request->email;
            $register->nomorHp = $request->nomorHp;
            $register->semester = $request->semester;
            $register->ipk = rand (2.9*10, 3.4*10) / 10;
            $register->beasiswa_id = $request->beasiswa_id;
            $register->file = '/storage/' . $filePath;
            $register->save();
            return redirect()->route('register.index')
            ->with('sukses','Data Register Beasiswa telah Ditambahkan.');
        }
    }
    public function update(Request $request, $id)
    {
            $request->validate([
                'name' => 'required|unique:register_beasiswas',
                'email' => 'required|regex:/(.+)@(.+)\.(.+)/i',
                'nomorHp' => 'required|numeric|digits:12',
                'semester' => 'required',
                'beasiswa_id' => 'required',
                ]);

            // if($request->file()) {
            // $fileName = time().'_'.$request->file->getClientOriginalName();
            // $filePath = $request->file('file')->storeAs('uploads', $fileName, 'public');
            $register = RegisterBeasiswa::find($id);
            $register->name = $request->name;
            $register->email = $request->email;
            $register->nomorHp = $request->nomorHp;
            $register->semester = $request->semester;
            $register->ipk = rand (2.9*10, 3.4*10) / 10;
            $register->beasiswa_id = $request->beasiswa_id;
            // $register->file = '/storage/' . $filePath;
            $register->save();
            return redirect()->route('register.index')
            ->with('sukses','Data Register Beasiswa telah Diubah.');
        // }
    }
    public function verifikasi(Request $request, $id)
    {
        $beasiswa = RegisterBeasiswa::find($id);
        $beasiswa->status_ajuan = 1;
        $beasiswa->save($id);
        dd($beasiswa);
            return redirect()->route('register.index')
        ->with('sukses','Data  Telah berhasil Di delete');
    }

    public function download($file){
        
        return response()->download(storage_path('/storage/app/public/uploads/'.$file));
     }

    public function show($id)
    {
        
        $registerbeasiswa = RegisterBeasiswa::find($id);

        return view('register-beasiswa.show',compact('registerbeasiswa'));
    }

    
    public function destroy(RegisterBeasiswa $beasiswa,$id)
    {

        $beasiswa->delete($id);
            return redirect()->route('register.index')
        ->with('sukses','Data  Telah berhasil Di delete');
    }
    
    public function edit($id)
    {
        $beasiswa = RegisterBeasiswa::find($id);
        $biasis = Beasiswa::all();

        return view('register-beasiswa.edit',compact('beasiswa','biasis','id'));
    }

    public function search(Request $request)
    {
        $keyword = $request->search;
        $data = RegisterBeasiswa::where('name', 'like', "%" . $keyword . "%")
        ->orWhere('email', 'like', "%" . $keyword . "%")
        ->orWhere('nomorHp', 'like', "%" . $keyword . "%")
        ->orWhere('semester', 'like', "%" . $keyword . "%")->paginate(5);
        return view('register-beasiswa.index', compact('data'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function register_cetak_pdf()
    {
    	$registerbeasiswa = RegisterBeasiswa::all();
 
    	$pdf = PDF::loadview('register-beasiswa.report-register-beasiswa',['registerbeasiswa'=>$registerbeasiswa]);
    	return $pdf->download('laporan-register-beasiswa-pdf');
    }

}
