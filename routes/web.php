<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BeasiswaController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\RegisterBeasiswaController;
use App\Http\Controllers\RegisterBesiswaUserController;
use App\Http\Controllers\HasilController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

Auth::routes();
/*------------------------------------------
--------------------------------------------
All Normal Users Routes List
--------------------------------------------
--------------------------------------------*/
Route::middleware(['auth', 'user-access:user'])
->name('user.')
->group(function () {
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/user/hasil', [HasilController::class, 'index'])->name('hasil.data');

//Register Beasiswa
Route::resource('user/register', RegisterBesiswaUserController::class);
Route::get('/user/searchRegister', [RegisterBesiswaUserController::class, 'search'])->name('search.register');
Route::get('/user/cetak_register', [RegisterBesiswaUserController::class, 'register_cetak_pdf'])->name('cetak.register');
Route::get('/user/files/{file}', [RegisterBesiswaUserController::class, 'download'])->name('download.file');
});
/*------------------------------------------
--------------------------------------------
All Admin Routes List
--------------------------------------------
--------------------------------------------*/
Route::middleware(['auth', 'user-access:admin'])->group(function () {
Route::get('/admin/home', [HomeController::class, 'adminHome'])->name('admin.home');
Route::get('/admin/hasil', [HasilController::class, 'index'])->name('admin.hasil.data');

//Beasiswa Master Route
Route::resource('admin/beasiswa', BeasiswaController::class);
Route::get('/admin/dataBeasiswa', [BeasiswaController::class, 'index'])->name('admin.beasiswa.master');
Route::get('/beasiswa', [BeasiswaController::class, 'json'])->name('beasiswaJson');
Route::get('/admin/cetak_beasiswa', [BeasiswaController::class, 'cetak_pdf'])->name('admin.cetak.beasiswa');
Route::get('/admin/search', [BeasiswaController::class, 'search'])->name('admin.search');

//Register Beasiswa
Route::resource('beasiswa/register', RegisterBeasiswaController::class);
Route::get('/beasiswa/searchRegister', [RegisterBeasiswaController::class, 'search'])->name('admin.search.register');
Route::get('/cetak_register', [RegisterBeasiswaController::class, 'register_cetak_pdf'])->name('admin.cetak.register');
Route::get('/files/{file}', [RegisterBeasiswaController::class, 'download'])->name('admin.download.file');
Route::post('/beasiswa/verifikasi', [RegisterBeasiswaController::class, 'verifikasi'])->name('admin.verifikasi');

 });
