<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register_beasiswas', function (Blueprint $table) {
            $table->id();
            $table->string('name', 50);
            $table->string('email', 50);
            $table->string('nomorHp', 50);
            $table->string('semester', 50);
            $table->float('ipk');
            $table->unsignedBigInteger('beasiswa_id');
            $table->string('file')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->boolean('status_ajuan')->default(0);


            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('beasiswa_id')->references('id')->on('beasiswas')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('register_beasiswas');
    }
};
