<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Beasiswa>
 */
class BeasiswaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'beasiswa_name' => Str::random(10),
            'deskripsi' => fake()->text($maxNbChars = 50),
            'jenis' => fake()->randomElement(['Akademik', 'Non Akademik']),
            'donatur' => fake()->text($maxNbChars = 20),
        ];
    }
}
