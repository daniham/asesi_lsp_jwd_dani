@extends('layouts.app')
@section('content')
<style>
    textarea {
  white-space: pre;
  overflow-wrap: normal;
  overflow-x: scroll;
}
</style>
<div class="container mt-2">
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
        <h2>Edit Beasiswa</h2>
        </div>
        </div>
    </div>
    @if(session('status'))
    <div class="alert alert-success mb-1 mt-1">
    {{ session('status') }}
    </div>
@endif
    <form action="{{ route('beasiswa.update',$beasiswa->id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            <strong>Nama Beasiswa:</strong>
            <input type="text" name="beasiswa_name" value="{{ $beasiswa->beasiswa_name }}" class="form-control" placeholder="Nama Beasiswa">
            @error('beasiswa_name')
            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
            @enderror
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Deskripsi Beasiswa:</strong>
         
                <textarea style="white-space: normal;" id="trmp" name = "deskripsi" rows="10" cols="50" onKeyPress class="form-control">
                {{ $beasiswa->deskripsi }}
                </textarea>
                @error('deskripsi')
                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror
            </div>
        </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Jenis Beasiswa</strong>
                        <select class="form-control" id="jenis" name="jenis">
                            <option value="akademik" {{($beasiswa->jenis ==='akademik') ? 'selected' : ''}}> Akademik </option>
                            <option value="nonAkademik" {{($beasiswa->jenis ==='nonAkademik') ? 'selected' : ''}}> Non-Akademik </option>
                        </select>
                    
                        @error('target')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Donatur:</strong>
                <input type="text" name="donatur" value="{{ $beasiswa->donatur }}" class="form-control" placeholder="Donatur Beasiswa">
                @error('donatur')
                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror
            </div>
        </div>
        </div>
        <br>
    <button type="submit" class="btn btn-primary ml-3">Edit Beasiswa</button>
    <a class="btn btn-success" href="{{ route('beasiswa.index') }}"> Back</a>

    </div>
    </form>
</div>
@endsection
