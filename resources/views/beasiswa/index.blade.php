@extends('layouts.app')
@section('content')

<nav class=" container navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    @if(Auth::user()->type == "admin")
                        <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('beasiswa.index') }}">Master Data Beasiswa</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('register.index') }}"> Register Beasiswa</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('admin.hasil.data') }}">Hasil</a>
                    </li>
                    @else
                        <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('beasiswa.index') }}">Master Data Beasiswa</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('register.index') }}"> Register Beasiswa</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('admin.hasil.data') }}">Hasil</a>
                        </li>
                    @endif


                </ul>
            </div>
        </div>
    </nav>
<div class="container mt-2">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
            <h2> Input data Master Beasiswa</h2>
            </div>

            <div class="pull-right mb-2">
            <a class="btn btn-success" href="{{ route('beasiswa.create') }}"> Create Master Beasiswa</a>
            <a href="{{ route('admin.cetak.beasiswa') }}" class="btn btn-primary" target="_blank">Cetak Report</a>

            </div>
        </div>
        <form class="form" method="get" action="{{ route('admin.search') }}">
            <div class="form-group w-100 mb-3">
                <input type="text" name="search" class="form-control w-75 d-inline" id="search" placeholder="Masukkan pencarian data">
                <button type="submit" class="btn btn-primary mb-1">Cari</button>
            </div>
        </form>
        <!-- Start kode untuk form pencarian -->
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
    </div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
<p>{{ $message }}</p>
</div>
@endif
<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Beasiswa</th>
        <th>Deskripsi</th>
        <th>Jenis</th>
        <th>Donatur</th>
        <th width="280px">Action</th>
    </tr>
            @php $i=1 @endphp
@foreach ($data as $beasiswa)
<tr>
    <td>{{ $i++ }}</td>
    <td>{{ $beasiswa->beasiswa_name }}</td>
    <td>{{ $beasiswa->deskripsi }}</td>
    <td>{{ $beasiswa->jenis }}</td>
    <td>{{ $beasiswa->donatur }}</td>
    <td>
    <form action="{{ route('beasiswa.destroy',$beasiswa->id) }}" method="Post">
        <a class="btn btn-primary" href="{{ route('beasiswa.edit',$beasiswa->id) }}">Edit</a>
        <a class="btn btn-info" href="{{ route('beasiswa.show',$beasiswa->id) }}">Details</a>
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger">Delete</button>
    </form>
    </td>
</tr>
@endforeach
</table>
{!! $data->links() !!}
@endsection
