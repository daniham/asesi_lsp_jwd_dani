@php
    use Illuminate\Support\Carbon;

@endphp
<!DOCTYPE html>
<html>
<head>
	<title>Laporan Beasiswa</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<style>
    hr{
        border: 0;
        border-top: 3px double #8c8c8c;
    }
</style>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Data Beasiswa Terupdate</h4>
	</center>
    <hr>
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
                <th>Beasiswa</th>
                <th>Deskripsi</th>
                <th>Jenis</th>
                <th>Donatur</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($beasiswa as $p)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{$p->beasiswa_name}}</td>
				<td>{{$p->deskripsi}}</td>
				<td>{{$p->jenis}}</td>
				<td>{{$p->donatur}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
    <small class="pull-right">Bandung ,{{ Carbon::today()->format('l jS \\of F Y')}}</small>
    <br>
    <br>
    <br>
    <small>{{ Auth::user()->name }}</small>
</body>
</html>