@extends('layouts.app')
@section('content')

    <div class="container mt-2">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left mb-2">
                    <h2>Tambah Beasiswa</h2>
                </div>
            </div>
        </div>
        <br>
        @if(session('status'))
        <div class="alert alert-success mb-1 mt-1">
        {{ session('status') }}
        </div>
        @endif
        <form action="{{ route('beasiswa.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Nama Beasiswa:</strong>
                        <input type="text" name="beasiswa_name" class="form-control" placeholder="Nama Beasiswa">
                        @error('beasiswa_name')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Deskripsi Beasiswa:</strong>
                        <textarea id="trmp" style="white-space: normal;" type="text" class="form-control" name="deskripsi" placeholder="Deskripsi Beasiswa">
                        </textarea>
                        @error('deskripsi')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Jenis Beasiswa</strong>

                        <select name="jenis" class="form-control custom-select @error('target') is-invalid @enderror">
                            <option value="" selected disabled hidden>Pilih Jenis Beasiswa</option>
                            <option value="akademik" @if(old( 'target')=='akademik' ) selected @endif>Akademik</option>
                            <option value="nonAkademik" @if(old( 'target')=='nonAkademik' ) selected @endif>Non-Akademik</option>
                        </select>
                        @error('target')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Donatur:</strong>
                        <input type="text" name="donatur" class="form-control" placeholder="Nama Donatur">
                        @error('donatur')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>
            <br>
                <button type="submit" class="btn btn-primary ml-3">Tambah</button>
                <input class="btn btn-warning" type="reset" value="reset">
                <a class="btn btn-success" href="{{ route('beasiswa.index') }}"> Back</a>
        </form>
        </div>

@endsection
