@extends('layouts.app')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div>
                <h2> Details Beasiswa</h2>
            </div>
            <div>
                <a class="btn btn-primary" href="{{ route('beasiswa.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Beasiswa:</strong>
                {{ $beasiswa->beasiswa_name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Deskripsi:</strong>
                {{ $beasiswa->deskripsi}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Jenis:</strong>
                {{ $beasiswa->jenis}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Donatur:</strong>
                {{ $beasiswa->jenis}}
            </div>
        </div>
    </div>
@endsection