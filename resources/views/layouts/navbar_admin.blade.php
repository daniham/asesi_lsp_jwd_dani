

<!DOCTYPE html>
    <html lang="en">
    
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>LaraPost</title>
        {{-- import file bootstrap  --}}
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    </head>
    
    <body>
        <br>
        <div class="container">
            <nav class="navbar navbar-dark bg-primary">
                <div class="container">
                    <h1>Latihan Datatable Laravel</h1>
                </div>
            </nav>
            <br><br>
            <table class="table table-bordered" id="users-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Beasiswa</th>
                        <th>Deskripsi</th>
                        <th>Jenis</th>
                        <th>Donatur</th>
                    </tr>
                </thead>
            </table>
        </div>
        <!-- jQuery -->
        <script src="//code.jquery.com/jquery.js"></script>
        <!-- DataTables -->
        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script>
            $(function() {
                $('#users-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: 'beasiswa',
                    columns: [
                        { data: 'no', name:'id', render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }},
                        { data: 'beasiswa_name', name: 'beasiswa_name' },
                        { data: 'deskripsi', name: 'deskripsi' },
                        { data: 'beasiswa_name', name: 'beasiswa_name' },
                        { data: 'donatur', name: 'donatur'}
                    ]
                });
            });
        </script>
    </body>
    
    </html>