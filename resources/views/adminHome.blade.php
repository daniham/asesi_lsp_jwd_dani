@extends('layouts.app')
@section('content')

<div class="container">
    <nav class=" container navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="{{ route('beasiswa.index') }}">Master Data Beasiswa</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="{{ route('register.index') }}"> Register Beasiswa</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="{{ route('admin.hasil.data') }}">Hasil</a>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
    <br>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard Admin</div>
                <div class="card-body">
                    @if(session('login-success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('login-success') }}
                        </div>
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <b><h3>Selamat Datang di Sistem Informasi Beasiswa ,{{ Auth::user()->name }}</h3></b>
                    anda login sebagai <b>{{ Auth::user()->type }}</b>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection