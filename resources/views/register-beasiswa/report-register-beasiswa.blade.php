@php
    use Illuminate\Support\Carbon;

@endphp
<!DOCTYPE html>
<html>
<head>
	<title>Laporan Pendaftar Beasiswa</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<style>
    hr{
        border: 0;
        border-top: 3px double #8c8c8c;
    }
</style>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Pendaftar Beasiswa Terupdate</h4>
	</center>
    <hr>
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>Nama</th>
				<th>Email</th>
				<th>Nomor Handphone</th>
				<th>Semester</th>
				<th>IPK</th>
				<th>Beasiswa</th>
				<th>Status Ajuan</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($registerbeasiswa as $register)
			@php
			$child = DB::table('beasiswas')->select('beasiswa_name','register_beasiswas.beasiswa_id')
				->join('register_beasiswas','register_beasiswas.beasiswa_id','=','beasiswas.id')
				->where('beasiswa_id',$register->beasiswa_id)->first();
			@endphp
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{ $register->name }}</td>
				<td>{{ $register->email }}</td>
				<td>{{ $register->nomorHp }}</td>
				<td>{{ $register->semester }}</td>
				<td>
					@if($register->ipk ==null)
						<b>-</b>
					@else
						<b>{{ $register->ipk }}</b>
					@endif
				</td>
				<td>{{ $child->beasiswa_name }}</td>
				
				<td>
					@if($register->status_ajuan ==0)
						<b>Belum Diverifikasi</b>
					@else
						<b>Sudah Diverifikasi</b>
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
    <small class="pull-right">Bandung ,{{ Carbon::today()->format('l jS \\of F Y')}}</small>
    <br>
    <br>
    <br>
    <small>{{ Auth::user()->name }}</small>
</body>
</html>