@extends('layouts.app')
@section('content')
<style>
    textarea {
  white-space: pre;
  overflow-wrap: normal;
  overflow-x: scroll;
}
</style>
<div class="container mt-2">
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
        <h2>Edit Permohonan Beasiswa</h2>
        </div>
        </div>
    </div>
    @if(session('status'))
    <div class="alert alert-success mb-1 mt-1">
    {{ session('status') }}
    </div>
    @endif
    <form action="{{ route('register.update',$id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            <strong>Nama Mahasiswa:</strong>
            <input type="text" name="name" value="{{ $beasiswa->name }}" class="form-control" placeholder="Nama Beasiswa">
            @error('name')
            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
            @enderror
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            <strong>Email:</strong>
            <input type="email" name="email" value="{{ $beasiswa->email }}" class="form-control" placeholder="Nama Beasiswa">
            @error('email')
            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
            @enderror
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            <strong>Handphone:</strong>
            <input type="number" name="nomorHp" value="{{ $beasiswa->nomorHp }}" class="form-control" placeholder="Nama Beasiswa">
            @error('nomorHp')
            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
            @enderror
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            <strong>Semester:</strong>
            <select class="form-control" id="semester" name="semester">
                <option value="">Pilij Semester</option>
                <option value="1" {{ $beasiswa->semester == 1 ? 'selected' : '' }}>Satu</option>
                <option value="2" {{ $beasiswa->semester == 2 ? 'selected' : '' }}>Dua</option>
                <option value="3" {{ $beasiswa->semester == 3 ? 'selected' : '' }}>Tiga</option>
                <option value="4" {{ $beasiswa->semester == 4 ? 'selected' : '' }}>Empat</option>
                <option value="5" {{ $beasiswa->semester == 5 ? 'selected' : '' }}>Lima</option>
                <option value="6" {{ $beasiswa->semester == 6 ? 'selected' : '' }}>Enam</option>
                <option value="7" {{ $beasiswa->semester == 7 ? 'selected' : '' }}>Tujuh</option>
                <option value="8" {{ $beasiswa->semester == 8 ? 'selected' : '' }}>Delapan</option>
            </select>
            @error('semester')
            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
            @enderror
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            <strong>Beasiswa:</strong>
            <select name="beasiswa_id" id="beasiswa_id"
                        class="form-control custom-select @error('beasiswa_id') is-invalid @enderror">
                        <option value="">Pilih Beasiswa</option>
                        @foreach ($biasis as $key => $category)
                            <option {{ old('beasiswa_id', $beasiswa->beasiswa_id) == $category->id ? 'selected' : '' }}
                                value="{{ $category->id }}">{{ $category->beasiswa_name }}</option>
                        @endforeach
                    </select>
            @error('beasiswa_id')
            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
            @enderror
            </div>
        </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>IPK:</strong>
                        <input type="text" name="ipk" class="form-control" placeholder="Nama IPK"
                         value={{rand (2.9*10, 3.4*10) / 10}} disabled="true">
                        @error('ipk')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Upload Berkas:</strong>
                        <input type="file" name="file" class="form-control custom-file-input" value="{{ substr($beasiswa->file,17) }}" id="chooseFile">
                        @error('beasiswa_id')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
    </div>

        <br>
    <button type="submit" class="btn btn-primary ml-3">Edit Beasiswa</button>
    <a class="btn btn-success" href="{{ route('register.index') }}"> Back</a>

    </div>
    </form>
</div>
@endsection
