@extends('layouts.app')
@section('content')

<nav class=" container navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                @if(Auth::user()->type != "admin")
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('user.register.index') }}">Add Beasiswa</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('user.hasil.data') }}">Hasil</a>
                    </li>

                @else
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('beasiswa.index') }}">Master Data Beasiswa</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('register.index') }}"> Register Beasiswa</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('admin.hasil.data') }}">Hasil</a>
                    </li>
                @endif
           

                </ul>
            </div>
        </div>
    </nav>
<div class="container mt-2">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
            <h2> Input Register Beasiswa</h2>
            </div>

            <div class="pull-right mb-2">
            @if(Auth::user()->type != "admin")
            <a class="btn btn-success" href="{{ route('user.register.create') }}"> Create Register Beasiswa</a>
            <a href="{{ route('user.cetak.register') }}" class="btn btn-primary" target="_blank">Cetak Report</a>

            @else
            <a class="btn btn-success" href="{{ route('register.create') }}"> Create Register Beasiswa</a>
            <a href="{{ route('admin.cetak.register') }}" class="btn btn-primary" target="_blank">Cetak Report</a>

            @endif
           

            </div>
        </div>
        @if(Auth::user()->type != "admin")
        <form class="form" method="get" action="{{ route('user.search.register') }}">

        @else
        <form class="form" method="get" action="{{ route('admin.search.register') }}">

        @endif
            <div class="form-group w-100 mb-3">
                <input type="text" name="search" class="form-control w-75 d-inline" id="search" placeholder="Masukkan pencarian data">
                <button type="submit" class="btn btn-primary mb-1">Cari</button>
            </div>
        </form>
        <!-- Start kode untuk form pencarian -->
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
    </div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
<p>{{ $message }}</p>
</div>
@endif
<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Nomor Handphone</th>
        <th>Semester</th>
        <th>IPK</th>
        <th>Beasiswa</th>
        <th>File</th>
        <th>Status Ajuan</th>
        <th width="280px">Action</th>
    </tr>
        @php $i=1 @endphp

        @foreach ($data as $register)
        @php
        $child = DB::table('beasiswas')->select('beasiswa_name','register_beasiswas.beasiswa_id')
            ->join('register_beasiswas','register_beasiswas.beasiswa_id','=','beasiswas.id')
            ->where('beasiswa_id',$register->beasiswa_id)->first();
        @endphp
<tr>
    <td>{{ $i++ }}</td>
    <td>{{ $register->name }}</td>
    <td>{{ $register->email }}</td>
    <td>{{ $register->nomorHp }}</td>
    <td>{{ $register->semester }}</td>
    <td>
        @if($register->ipk ==null)
            <b>-</b>
        @else
            <b>{{ $register->ipk }}</b>
        @endif
    </td>
    <td>{{ $child->beasiswa_name }}</td>
    <td>
        @if(Auth::user()->type != "admin")
            <a href="{!! route('user.download.file', substr($register->file,17)) !!}">{{substr($register->file,17)}}</a>

        @else
            <a href="{!! route('admin.download.file', substr($register->file,17)) !!}">{{substr($register->file,17)}}</a>
        @endif
    </td>
    <td>
        @if($register->status_ajuan ==0)
            <b>Belum Diverifikasi</b>
        @else
            <b>Sudah Diverifikasi</b>
        @endif
    </td>
    <td>
        @if(Auth::user()->type != "admin")
        <form action="{{ route('user.register.destroy',$register->id) }}" method="Post">

        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
        Edit
        </button>
        <a class="btn btn-info" href="{{ route('user.register.show',$register->id) }}">Details</a>
        @else
        <form action="{{ route('register.destroy',$register->id) }}" method="Post">

        <a class="btn btn-primary" href="{{ route('register.edit',$register->id) }}">Edit</a>
        <a class="btn btn-info" href="{{ route('register.show',$register->id) }}">Details</a>
        @endif
        @csrf
        @method('DELETE')
        <button style="display:none;" type="submit" class="btn btn-danger">Delete</button>
    </form>
    </td>
</tr>
@endforeach
</table>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pemberitahuan</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        Untuk melakukan perubahan data pemohon beasiswa silahkan hubungi admin..
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
{!! $data->links() !!}
@endsection
