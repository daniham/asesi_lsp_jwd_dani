@extends('layouts.app')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div>
                <h2> Details Pemohon Beasiswa</h2>
            </div>
            <div>
            @if(Auth::user()->type != "admin")
            <a class="btn btn-primary" href="{{ route('user.register.index') }}"> Back</a>

            @else
            <a class="btn btn-primary" href="{{ route('register.index') }}"> Back</a>

            @endif
            </div>
        </div>
    </div>
    @php
        $child = DB::table('beasiswas')->select('beasiswa_name','register_beasiswas.beasiswa_id')
            ->join('register_beasiswas','register_beasiswas.beasiswa_id','=','beasiswas.id')
            ->where('beasiswa_id',$registerbeasiswa->beasiswa_id)->first();
        @endphp
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama:</strong>
                {{ $registerbeasiswa->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                {{ $registerbeasiswa->email}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nomor Hp:</strong>
                {{ $registerbeasiswa->nomorHp}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Semester:</strong>
                
                {{ $registerbeasiswa->semester}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Ipk:</strong>
                @if($registerbeasiswa->ipk ==null)
                    <b>-</b>
                @else
                    <b>{{ $registerbeasiswa->ipk }}</b>
                @endif
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Beasiswa:</strong>
                    <b>{{ $child->beasiswa_name }}</b>
            </div>
        </div>
    </div>
@endsection