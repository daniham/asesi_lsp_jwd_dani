@extends('layouts.app')
@section('content')
    <div class="container mt-2">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left mb-2">
                    <h2>Tambah Register</h2>
                </div>
            </div>
        </div>
        <br>
        @if(session('status'))
        <div class="alert alert-success mb-1 mt-1">
        {{ session('status') }}
        </div>
        @endif
        @if(Auth::user()->type != "admin")
            <form action="{{ route('user.register.store') }}" method="POST" enctype="multipart/form-data">
        @else
            <form action="{{ route('register.store') }}" method="POST" enctype="multipart/form-data">
        @endif
            @csrf
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Nama Mahasiswa:</strong>
                        <input type="text" name="name" class="form-control" placeholder="Nama Mahasiswa">
                        @error('name')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Email:</strong>
                        <input type="email" name="email" class="form-control" placeholder="Email Mahasiswa">
                        @error('email')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Handphone:</strong>
                        <input type="number" name="nomorHp" class="form-control" placeholder="Nomor Handphone">
                        @error('nomorHp')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Semester</strong>

                        <select class="form-control" name="semester"  onclick="cek_data()" >
                            <option selected>Pilih Semester</option>
                            <option value="1">Satu</option>
                            <option value="2">Dua</option>
                            <option value="3">Tiga</option>
                            <option value="4">Empat</option>
                            <option value="5">Lima</option>
                            <option value="6">Enam</option>
                            <option value="7">Tujuh</option>
                            <option value="8">Delapan</option>
                        </select>
                        @error('target')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>IPK:</strong>
                        <input type="text" id="ipk" name="ipk" class="form-control" placeholder="Nama IPK"
                         value={{rand (2.9*10, 3.4*10) / 10}} disabled="true">
                        @error('ipk')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Beasiswa:</strong>
                        <select class="form-control" id="beasiswa_id" name="beasiswa_id">
                        @foreach ($beasiswa as $beasis)
                            <option value="{{ $beasis->id }}">{{ $beasis->beasiswa_name }}</option>
                        @endforeach
                        </select>
                        @error('beasiswa_id')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Upload Berkas:</strong>
                        <input type="file" name="file" class="form-control custom-file-input" id="chooseFile">
                        @error('beasiswa_id')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

            </div>
            <br>

                <button type="submit" class="btn btn-primary ml-3">Tambah</button>
                <input class="btn btn-warning" type="reset" value="reset">
                @if(Auth::user()->type == "admin")
                    <a class="btn btn-success" href="{{ route('register.index') }}"> Back</a>
                @else
                    <a class="btn btn-success" href="{{ route('user.register.store') }}"> Back</a>
                @endif
        </form>
        </div>
       
        <script type="text/javascript">
            function cek_data(){
            var ipk = document.getElementById("ipk").value;
            if (ipk ==3 || ipk <= 3) {
                // console.log("disabled")
                document.getElementById("chooseFile").disabled = true;
                document.getElementById("beasiswa_id").disabled = true;
            }else{
                // console.log("enabled");
                document.getElementById("chooseFile").disabled = false;
                document.getElementById("beasiswa_id").disabled = false;
            }
            console.log(ipk);
            }
    </script>
@endsection
